import { IthinkaTemplatePage } from './app.po';

describe('Ithinka App', function() {
  let page: IthinkaTemplatePage;

  beforeEach(() => {
    page = new IthinkaTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
