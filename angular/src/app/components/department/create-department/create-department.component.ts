import { Component, Injector, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { DepartmentService } from '@app/services/department/department.service';
import { CreateDepartmentInput } from '@app/services/department/dtos/CreateDepartmentDto';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryPartOutPut } from '@app/services/factory/dtos/FactoryPartOutPut';

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.css']
})
export class CreateDepartmentDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  department: CreateDepartmentInput = new CreateDepartmentInput();
  factories: FactoryPartOutPut[] = [];
  selectedFactory: FactoryFullOutPut;

  constructor(injector: Injector,
    public _departmentService: DepartmentService,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<CreateDepartmentDialogComponent>) {
    super(injector);
  }
  ngOnInit(): void {
    this.department.name ="Yeni Departman";
    this.getFactories();
  }

  save(): void {
    this.saving = true;

    this._departmentService
      .create(this.department)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  
  getFactories(): FactoryFullOutPut[] | any{
    this._factoryService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: FactoryFullOutPut[]) => { 
        return this.factories = result;
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}
