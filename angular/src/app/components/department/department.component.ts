import { Component, Injector, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DepartmentService } from '@app/services/department/department.service';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { finalize } from 'rxjs/operators';
import { DeleteDepartmentInput } from '@app/services/department/dtos/DeleteDepartmentInput';
import { HttpClient } from "@angular/common/http";
import DataSource from "devextreme/data/data_source";
import CustomStore from 'devextreme/data/custom_store';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./department.component.css'],
  providers: []
})
export class DepartmentComponent extends AppComponentBase implements OnInit {
  departments: DepartmentFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _departmentService: DepartmentService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createDepartment(): void {
    this._modelManagerService.openCreateDepartmentDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editDepartment(id: number): void {
    this._modelManagerService.openEditDepartmentDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteDepartment(department: DeleteDepartmentInput): void {
    abp.message.confirm(
      this.l('DepartmentDeleteWarningMessage', department.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._departmentService
            .delete(department)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
    
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._departmentService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                  //finishedCallback();
                })
              )
              .subscribe((result: DepartmentFullOutPut[]) => {
                this.departments = result;
                resolve(this.departments);
              });
          });
        }
      })
    });
  }
}
