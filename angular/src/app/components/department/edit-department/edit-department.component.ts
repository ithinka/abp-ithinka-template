import { Component, Injector, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { DepartmentService } from '@app/services/department/department.service';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { GetDepartmentInput } from '@app/services/department/dtos/GetDepartmentInput';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';

@Component({
  selector: 'app-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.css']
})
export class EditDepartmentDialogComponent extends AppComponentBase implements OnInit {
  saving = false;
  department: DepartmentFullOutPut = new DepartmentFullOutPut();
  factories: FactoryFullOutPut[] = [];

  constructor(injector: Injector,
    public _departmentService: DepartmentService,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<EditDepartmentDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _department: GetDepartmentInput) {
    super(injector);
  }

  ngOnInit() {
    this._departmentService.get(this._department).subscribe((result: DepartmentFullOutPut) => {
      this.department = result;
    });
    this.getFactories();
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._departmentService
      .update({name:this.department.name, id: this.department.id, factory: this.department.factory})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  getFactories(): FactoryFullOutPut[] | any{
    this._factoryService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: FactoryFullOutPut[]) => { 
        return this.factories = result;
      });
  }
  // Mat Select Componenti için Two Binding yapılmak istediğinde 
  // Form açıldığında seçili olan veriyi göstermek için compare işlemei yapılmalı
  customCompare(o1, o2) {
    return o1.id === o2.id;
  }
  
  close(result: any): void {
    this._dialogRef.close(result);
  }
}
