import { Component, OnInit, Injector } from '@angular/core';
import { CreateDeviceGroupInput } from '@app/services/device-group/dtos/CreateDeviceGroupInput';
import { DepartmentPartOutPut } from '@app/services/department/dtos/DepartmentPartOutPut';
import { DepartmentService } from '@app/services/department/department.service';
import { DeviceGroupService } from '@app/services/device-group/device-group.service';
import { MatDialogRef } from '@angular/material';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';

@Component({
  selector: 'app-create-device-group',
  templateUrl: './create-device-group.component.html',
  styleUrls: ['./create-device-group.component.css']
})
export class CreateDeviceGroupComponent extends AppComponentBase
implements OnInit {
saving = false;
deviceGroup: CreateDeviceGroupInput = new CreateDeviceGroupInput();
departments: DepartmentPartOutPut[] = [];
factories: FactoryFullOutPut[] = [];
selectedFactory: FactoryFullOutPut;

constructor(injector: Injector,
  public _departmentService: DepartmentService,
  public _deviceGroupService: DeviceGroupService,
  public _factoryService: FactoryService,
  private _dialogRef: MatDialogRef<CreateDeviceGroupComponent>) {
  super(injector);
}
ngOnInit(): void {
  this.deviceGroup.name ="Yeni Makine Grup";
  this.getDepartments();
  this.getFactories();
}

save(): void {
  this.saving = true;
  this._deviceGroupService
    .create(this.deviceGroup)
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    )
    .subscribe(() => {
      this.notify.info(this.l('SavedSuccessfully'));
      this.close(true);
    });
}


getDepartments(): DepartmentFullOutPut[] | any{
  this._departmentService
    .getList()
    .pipe(
      finalize(() => {
        return null;
      })
    )
    .subscribe((result: DepartmentFullOutPut[]) => { 
      return this.departments = result;
    });
}

getFactories(): FactoryFullOutPut[] | any {
  this._factoryService
  .getList()
  .pipe(
    finalize(() =>{
      return null;
    })
  )
  .subscribe((result: FactoryFullOutPut[]) => {
    return this.factories = result;
  })
}

close(result: any): void {
  this._dialogRef.close(result);
}

}
