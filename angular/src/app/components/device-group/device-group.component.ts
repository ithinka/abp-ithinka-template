import { Component, OnInit, Injector, Inject } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DeviceGroupFullOutPut } from '@app/services/device-group/dtos/DeviceGroupFullOutPut';
import { AppComponentBase } from '@shared/app-component-base';
import { DeviceGroupService } from '@app/services/device-group/device-group.service';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { DeleteDeviceGroupInput } from '@app/services/device-group/dtos/DeleteDeviceGroupInput';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-device-group',
  templateUrl: './device-group.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./device-group.component.css'],
  providers: []
})
export class DeviceGroupComponent extends AppComponentBase implements OnInit {
  deviceGroups: DeviceGroupFullOutPut[] = [];
  dataSource: any = {};

  constructor(
    injector: Injector,
    private _deviceGroupService: DeviceGroupService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createDeviceGroup(): void {
    this._modelManagerService.openCreateDeviceGroupDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editDeviceGroup(id: number): void {
    this._modelManagerService.openEditDeviceGroupDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteDeviceGroup(deviceGroup: DeleteDeviceGroupInput): void {
    abp.message.confirm(
      this.l('DeviceGroupDeleteWarningMessage', deviceGroup.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._deviceGroupService
            .delete(deviceGroup)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
    
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._deviceGroupService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                  //finishedCallback();
                })
              )
              .subscribe((result: DeviceGroupFullOutPut[]) => {
                this.deviceGroups = result;
                resolve(this.deviceGroups);
              });
          });
        }
      })
    });
  }
}
