import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { DeviceGroupFullOutPut } from '@app/services/device-group/dtos/DeviceGroupFullOutPut';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { DepartmentService } from '@app/services/department/department.service';
import { DeviceGroupService } from '@app/services/device-group/device-group.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GetDeviceGroupInput } from '@app/services/device-group/dtos/GetDeviceGroupInput';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryPartOutPut } from '@app/services/factory/dtos/FactoryPartOutPut';

@Component({
  selector: 'app-edit-device-group',
  templateUrl: './edit-device-group.component.html',
  styleUrls: ['./edit-device-group.component.css']
})
export class EditDeviceGroupComponent extends AppComponentBase implements OnInit {
  saving = false;
  deviceGroup: DeviceGroupFullOutPut = new DeviceGroupFullOutPut();
  departments: DepartmentFullOutPut[] = [];
  factories: FactoryFullOutPut[] = [];
  selectedFactory: FactoryPartOutPut;

  constructor(injector: Injector,
    public _departmentService: DepartmentService,
    public _deviceGroupService: DeviceGroupService,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<EditDeviceGroupComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _deviceGroup: GetDeviceGroupInput) {
    super(injector);
  }

  ngOnInit() {
    this._deviceGroupService.get(this._deviceGroup).subscribe((result: DeviceGroupFullOutPut) => {
      this.deviceGroup = result;
      this.selectedFactory = result.department.factory;
    });
    this.getDepartments();
    this.getFactories();
  }

  save(): void {
    this.saving = true;
    this._deviceGroupService
      .update({name:this.deviceGroup.name, ipAddress:this.deviceGroup.ipAddress, id: this.deviceGroup.id, department: this.deviceGroup.department})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  getDepartments(): DepartmentFullOutPut[] | any{
    this._departmentService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DepartmentFullOutPut[]) => { 
        return this.departments = result;
      });
  }

  getFactories(): FactoryFullOutPut[] | any {
    this._factoryService
    .getList()
    .pipe(
      finalize(() =>{
        return null;
      })
    )
    .subscribe((result: FactoryFullOutPut[]) => {
      return this.factories = result;
    })
  }

  // Mat Select Componenti için Two Binding yapılmak istediğinde 
  // Form açıldığında seçili olan veriyi göstermek için compare işlemei yapılmalı
  customCompare(o1, o2) {
    return o1.id === o2.id;
  }
  
  close(result: any): void {
    this._dialogRef.close(result);
  }
}
