import { Component, OnInit, Injector } from '@angular/core';
import { DepartmentService } from '@app/services/department/department.service';
import { DeviceGroupService } from '@app/services/device-group/device-group.service';
import { MatDialogRef } from '@angular/material';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { CreateDeviceInput } from '@app/services/device/dtos/CreateDeviceInput';
import { DeviceService } from '@app/services/device/device.service';
import { DeviceGroupFullOutPut } from '@app/services/device-group/dtos/DeviceGroupFullOutPut';

@Component({
  selector: 'app-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.css']
})
export class CreateDeviceComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  device: CreateDeviceInput = new CreateDeviceInput();
  factories: FactoryFullOutPut[] = [];
  selectedFactory: FactoryFullOutPut;
  departments: DepartmentFullOutPut[] = [];
  selectedDepartment: DepartmentFullOutPut;
  deviceGroups: DeviceGroupFullOutPut[] = [];

  constructor(injector: Injector,
    public _deviceService: DeviceService,
    public _factoryService: FactoryService,
    public _deviceGroupService: DeviceGroupService,
    public _departmentService: DepartmentService,
    private _dialogRef: MatDialogRef<CreateDeviceComponent>) {
    super(injector);
  }
  ngOnInit(): void {
    this.device.name = "Yeni Makine";
    this.getDepartments();
    this.getFactories();
    this.getDeviceGroups();
  }

  save(): void {
    this.saving = true;
    this._deviceService
      .create(this.device)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }


  getDepartments(): DepartmentFullOutPut[] | any {
    this._departmentService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DepartmentFullOutPut[]) => {
        return this.departments = result;
      });
  }

  getFactories(): FactoryFullOutPut[] | any {
    this._factoryService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: FactoryFullOutPut[]) => {
        return this.factories = result;
      })
  }

  getDeviceGroups(): DeviceGroupFullOutPut[] | any {
    this._deviceGroupService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceGroupFullOutPut[]) => {
        return this.deviceGroups = result;
      })
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }

}

