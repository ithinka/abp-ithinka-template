import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { DeviceGroupFullOutPut } from '@app/services/device-group/dtos/DeviceGroupFullOutPut';
import { DepartmentFullOutPut } from '@app/services/department/dtos/DepartmentFullOutPut';
import { DepartmentService } from '@app/services/department/department.service';
import { DeviceGroupService } from '@app/services/device-group/device-group.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GetDeviceGroupInput } from '@app/services/device-group/dtos/GetDeviceGroupInput';
import { AppComponentBase } from '@shared/app-component-base';
import { finalize } from 'rxjs/operators';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryPartOutPut } from '@app/services/factory/dtos/FactoryPartOutPut';
import { DepartmentPartOutPut } from '@app/services/department/dtos/DepartmentPartOutPut';
import { DeviceService } from '@app/services/device/device.service';
import { GetDeviceInput } from '@app/services/device/dtos/GetDeviceInput';
import { DeviceFullOutPut } from '@app/services/device/dtos/DeviceFullOutPut';

@Component({
  selector: 'app-edit-device',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.css']
})
export class EditDeviceComponent extends AppComponentBase implements OnInit {
  saving = false;
  device: DeviceFullOutPut = new DeviceFullOutPut();
  deviceGroups: DeviceGroupFullOutPut[] = [];
  departments: DepartmentFullOutPut[] = [];
  selectedDepartment: DepartmentPartOutPut;
  factories: FactoryFullOutPut[] = [];
  selectedFactory: FactoryPartOutPut;

  constructor(injector: Injector,
    public _deviceService: DeviceService,
    public _departmentService: DepartmentService,
    public _deviceGroupService: DeviceGroupService,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<EditDeviceComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _device: GetDeviceInput) {
    super(injector);
  }

  ngOnInit() {
    this._deviceService.get(this._device).subscribe((result: DeviceFullOutPut) => {
      this.device = result;
      this.selectedFactory = result.deviceGroup.department.factory;
      this.selectedDepartment = result.deviceGroup.department;
    });
    this.getDepartments();
    this.getFactories();
    this.getDeviceGroups();
  }

  save(): void {
    this.saving = true;
    this._deviceService
      .update({ name:this.device.name, 
        deviceNo:this.device.deviceNo, 
        isOpened: this.device.isOpened, 
        id: this.device.id, 
        deviceGroup: this.device.deviceGroup})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  getDepartments(): DepartmentFullOutPut[] | any{
    this._departmentService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DepartmentFullOutPut[]) => { 
        return this.departments = result;
      });
  }

  getFactories(): FactoryFullOutPut[] | any {
    this._factoryService
    .getList()
    .pipe(
      finalize(() =>{
        return null;
      })
    )
    .subscribe((result: FactoryFullOutPut[]) => {
      return this.factories = result;
    })
  }

  getDeviceGroups(): DeviceGroupFullOutPut[] | any {
    this._deviceGroupService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: DeviceGroupFullOutPut[]) => {
        return this.deviceGroups = result;
      })
  }

  // Mat Select Componenti için Two Binding yapılmak istediğinde 
  // Form açıldığında seçili olan veriyi göstermek için compare işlemei yapılmalı
  customCompare(o1, o2) {
    return o1.id === o2.id;
  }
  
  close(result: any): void {
    this._dialogRef.close(result);
  }
}
