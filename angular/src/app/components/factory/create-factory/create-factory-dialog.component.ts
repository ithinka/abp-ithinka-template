import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateFactoryInput } from '@app/services/factory/dtos/CreateFactoryInput';
import { FactoryService } from '@app/services/factory/factory.service';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-create-factory-dialog',
  templateUrl: './create-factory-dialog.component.html',
  styleUrls: ['./create-factory-dialog.component.css']
})
export class CreateFactoryDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  factory: CreateFactoryInput = new CreateFactoryInput();

  constructor(injector: Injector,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<CreateFactoryDialogComponent>) {
    super(injector);
  }

  ngOnInit(): void {
    this.factory.name = "Yeni Fabrika";
  }

  save(): void {
    this.saving = true;

    this._factoryService
      .create(this.factory)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });

  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
