import { Component, OnInit, Injector, Optional, Inject } from '@angular/core';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { FactoryService } from '@app/services/factory/factory.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GetFactoryInput } from '@app/services/factory/dtos/GetFactoryInput';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'app-edit-factory-dialog',
  templateUrl: './edit-factory-dialog.component.html',
  styleUrls: ['./edit-factory-dialog.component.css']
})
export class EditFactoryDialogComponent extends AppComponentBase implements OnInit {
  saving = false;
  factory: FactoryFullOutPut = new FactoryFullOutPut();;

  constructor(injector: Injector,
    public _factoryService: FactoryService,
    private _dialogRef: MatDialogRef<EditFactoryDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _factory: GetFactoryInput) {
    super(injector);
  }

  ngOnInit() {
    this._factoryService.get(this._factory).subscribe((result: FactoryFullOutPut) => {
      this.factory = result;
    });
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._factoryService
      .update({name:this.factory.name, id: this.factory.id})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
