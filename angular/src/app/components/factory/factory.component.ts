import { Component, Injector, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { finalize } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";
import DataSource from "devextreme/data/data_source";
import CustomStore from 'devextreme/data/custom_store';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { FactoryService } from '@app/services/factory/factory.service';
import { FactoryFullOutPut } from '@app/services/factory/dtos/FactoryFullOutPut';
import { DeleteFactoryInput } from '@app/services/factory/dtos/DeleteFactoryInput';

@Component({
  selector: 'app-factory',
  templateUrl: './factory.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./factory.component.css'],
  providers: []
})
export class FactoryComponent extends AppComponentBase implements OnInit {
  factories: FactoryFullOutPut[] = [];
  dataSource: any = {};

  constructor(injector: Injector,
    private _factoryService: FactoryService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
    ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createFactory(): void {
    this._modelManagerService.openCreateFactoryDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editFactory(id: number): void {
    this._modelManagerService.openEditFactoryDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteFactory(factory: DeleteFactoryInput): void {
    abp.message.confirm(
      this.l('FactoryDeleteWarningMessage', factory.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._factoryService
            .delete(factory)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._factoryService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: FactoryFullOutPut[]) => {
                this.factories = result;
                resolve(this.factories);
              });
          });
        }
      })
    });
  }

}
