import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';

import { CreateStopTypeInput } from '@app/services/stop-type/dtos/CreateStopTypeInput';
import { StopTypeService } from '@app/services/stop-type/stop-type.service';

@Component({
  selector: 'app-create-stop-type',
  templateUrl: './create-stop-type.component.html',
  styleUrls: ['./create-stop-type.component.css']
})
export class CreateStopTypeComponent extends AppComponentBase
implements OnInit {
saving = false;
stopType: CreateStopTypeInput = new CreateStopTypeInput();

constructor(injector: Injector,
  public _stopTypeService: StopTypeService,
  private _dialogRef: MatDialogRef<CreateStopTypeComponent>) {
  super(injector);
}

ngOnInit(): void {
  this.stopType.name = "Yeni Duruş Tipi";
}

save(): void {
  this.saving = true;

  this._stopTypeService
    .create(this.stopType)
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    )
    .subscribe(() => {
      this.notify.info(this.l('SavedSuccessfully'));
      this.close(true);
    });

}

close(result: any): void {
  this._dialogRef.close(result);
}
}
