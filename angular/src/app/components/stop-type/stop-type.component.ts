import { Component, Injector, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";

import DataSource from "devextreme/data/data_source";
import CustomStore from 'devextreme/data/custom_store';
import { AppComponentBase } from '@shared/app-component-base';
import { ModalManagerService } from '@app/services/common/modal-manager.service';
import { StopTypeService } from '@app/services/stop-type/stop-type.service';
import { StopTypeFullOutPut } from '@app/services/stop-type/dtos/StopTypeFullOutPut';
import { DeleteStopTypeInput } from '@app/services/stop-type/dtos/DeleteStopTypeInput';

@Component({
  selector: 'app-stop-type',
  templateUrl: './stop-type.component.html',
  styleUrls: ['./stop-type.component.css']
})
export class StopTypeComponent extends AppComponentBase implements OnInit {
  stopTypes: StopTypeFullOutPut[] = [];
  dataSource: any = {};

  constructor(injector: Injector,
    private _stopTypeService: StopTypeService,
    private _modelManagerService: ModalManagerService,
    private _dialog: MatDialog,
    @Inject(HttpClient) httpClient: HttpClient
    ) {
    super(injector);
  }

  ngOnInit(): void {
    this.dataSource = this.createDxDataSource();
  }

  createStopType(): void {
    this._modelManagerService.openCreateStopTypeDialog().afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  editStopType(id: number): void {
    this._modelManagerService.openEditStopTypeDialog(id).afterClosed().subscribe(result => {
      if (result) {
        this.refreshDataGrid();
      }
    });
  }

  deleteStopType(stopType: DeleteStopTypeInput): void {
    abp.message.confirm(
      this.l('StopTypeDeleteWarningMessage', stopType.name),
      this.l("AreYouSureWarningMessage"),
      (result: boolean) => {
        if (result) {
          this._stopTypeService
            .delete(stopType)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refreshDataGrid();
              })
            )
            .subscribe(() => { });
        }
      }
    );
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        template: 'formNameTemplate'
      },
      {
        location: 'after',
        template: 'refreshButtonTemplate'
      });
  }

  // dxGrid Changed
  onOptionChanged(e){
    // console.log(e);
  }

  refreshDataGrid() {
    this.dataSource.reload();
  }
  
  createDxDataSource(): DataSource {
    return new DataSource({
      store: new CustomStore({
        key: "id",
        loadMode: "raw",
        load: () => {
          return new Promise((resolve, reject) => {
            this._stopTypeService
              .getList()
              .pipe(
                finalize(() => {
                  reject();
                })
              )
              .subscribe((result: StopTypeFullOutPut[]) => {
                this.stopTypes = result;
                resolve(this.stopTypes);
              });
          });
        }
      })
    });
  }

}

