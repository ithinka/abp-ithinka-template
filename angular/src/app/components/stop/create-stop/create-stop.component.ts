import { Component, Injector, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { CreateStopInput } from '@app/services/stop/dtos/CreateStopInput';
import { StopTypeFullOutPut } from '@app/services/stop-type/dtos/StopTypeFullOutPut';
import { StopService } from '@app/services/stop/stop.service';
import { StopTypeService } from '@app/services/stop-type/stop-type.service';

@Component({
  selector: 'app-create-stop',
  templateUrl: './create-stop.component.html',
  styleUrls: ['./create-stop.component.css']
})
export class CreateStopComponent extends AppComponentBase
implements OnInit {
saving = false;
stop: CreateStopInput = new CreateStopInput();
stopTypes: StopTypeFullOutPut[] = [];
// selectedFactory: FactoryFullOutPut;

constructor(injector: Injector,
  public _stopService: StopService,
  public _stopTypeService: StopTypeService,
  private _dialogRef: MatDialogRef<CreateStopComponent>) {
  super(injector);
}
ngOnInit(): void {
  this.stop.name ="Yeni Duruş";
  this.getStopTypes();
}

save(): void {
  this.saving = true;

  this._stopService
    .create(this.stop)
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    )
    .subscribe(() => {
      this.notify.info(this.l('SavedSuccessfully'));
      this.close(true);
    });
}


getStopTypes(): StopTypeFullOutPut[] | any{
  this._stopTypeService
    .getList()
    .pipe(
      finalize(() => {
        return null;
      })
    )
    .subscribe((result: StopTypeFullOutPut[]) => { 
      return this.stopTypes = result;
    });
}

close(result: any): void {
  this._dialogRef.close(result);
}

}
