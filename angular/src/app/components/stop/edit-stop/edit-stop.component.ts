import { Component, Injector, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';
import { StopFullOutPut } from '@app/services/stop/dtos/StopFullOutPut';
import { StopTypeFullOutPut } from '@app/services/stop-type/dtos/StopTypeFullOutPut';
import { StopTypeService } from '@app/services/stop-type/stop-type.service';
import { StopService } from '@app/services/stop/stop.service';
import { GetStopInput } from '@app/services/stop/dtos/GetStopInput';

@Component({
  selector: 'app-edit-stop',
  templateUrl: './edit-stop.component.html',
  styleUrls: ['./edit-stop.component.css']
})
export class EditStopComponent extends AppComponentBase implements OnInit {
  saving = false;
  stop: StopFullOutPut = new StopFullOutPut();
  stopTypes: StopTypeFullOutPut[] = [];

  constructor(injector: Injector,
    public _stopService: StopService,
    public _stopTypeService: StopTypeService,
    private _dialogRef: MatDialogRef<EditStopComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _stop: GetStopInput) {
    super(injector);
  }

  ngOnInit() {
    this._stopService.get(this._stop).subscribe((result: StopFullOutPut) => {
      this.stop = result;
    });
    this.getStopTypes();
  }

  save(): void {
    this.saving = true;
    // let updateDepartmentInput = new UpdateDepartmentInput();
    // updateDepartmentInput.id = this.department.id;
    // updateDepartmentInput.name = this.department.name;
    this._stopService
      .update({name:this.stop.name, 
        id: this.stop.id, 
        duration:this.stop.duration, 
        stopType: this.stop.stopType})
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  getStopTypes(): StopTypeFullOutPut[] | any{
    this._stopTypeService
      .getList()
      .pipe(
        finalize(() => {
          return null;
        })
      )
      .subscribe((result: StopTypeFullOutPut[]) => { 
        return this.stopTypes = result;
      });
  }
  // Mat Select Componenti için Two Binding yapılmak istediğinde 
  // Form açıldığında seçili olan veriyi göstermek için compare işlemei yapılmalı
  customCompare(o1, o2) {
    return o1.id === o2.id;
  }
  
  close(result: any): void {
    this._dialogRef.close(result);
  }
}
