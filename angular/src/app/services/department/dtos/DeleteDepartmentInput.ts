export class DeleteDepartmentInput implements IDeleteDepartmentInput {
    name: string;
    id: number;
}

export interface IDeleteDepartmentInput{
    name: string;
    id: number;
}