export class DepartmentPartOutPut implements IDepartmentPartOutPut {
    name: string;
    id: number;
}

export interface IDepartmentPartOutPut {
    name: string;
    id: number;
}