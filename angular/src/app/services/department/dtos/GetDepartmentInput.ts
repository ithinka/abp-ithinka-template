export class GetDepartmentInput implements IGetDepartmentInput{
    id: number;
}

export interface IGetDepartmentInput{
    id:number;
}