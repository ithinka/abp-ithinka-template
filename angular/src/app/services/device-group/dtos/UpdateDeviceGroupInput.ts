import { DepartmentPartOutPut } from "@app/services/department/dtos/DepartmentPartOutPut";

export class UpdateDeviceGroupInput implements IUpdateDeviceGroupInput {
    name: string;
    ipAddress: string;
    id: number;
    department: DepartmentPartOutPut;
}

export interface IUpdateDeviceGroupInput {
    name: string | undefined;
    ipAddress: string;
    id:number
    department: DepartmentPartOutPut
}