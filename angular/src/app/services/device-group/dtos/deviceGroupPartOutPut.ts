export class DeviceGroupPartOutPut implements IDeviceGroupPartOutPut {
    name: string;
    ipAddres: string;
    id: number;
}

export interface IDeviceGroupPartOutPut {
    name: string;
    ipAddres: string;
    id:number;
}