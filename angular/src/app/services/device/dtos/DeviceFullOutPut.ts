import { isObject } from "util";
import { DeviceGroupFullOutPut } from "@app/services/device-group/dtos/DeviceGroupFullOutPut";

export class DeviceFullOutPut implements IDeviceFullOutPut {
    name:string;
    deviceNo: string;
    id: number;
    isOpened: boolean;
    deviceGroup: DeviceGroupFullOutPut;

    constructor(data?: IDeviceFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
                this.deviceNo = data.deviceNo;
                this.isOpened = data.isOpened;
                this.deviceGroup = data.deviceGroup;
            }
        }
    }

    static fromJS(data: any): DeviceFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new DeviceFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        data["deviceNo"] = this.deviceNo;
        data["deviceGroup"] = this.deviceGroup;
        data["isOpened"] = this.isOpened;
        return data; 
    }

    clone(): DeviceFullOutPut {
        const json = this.toJSON();
        let result = new DeviceFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IDeviceFullOutPut {
    name: string | undefined;
    deviceNo: string;
    isOpened: boolean;
    id:number
    deviceGroup: DeviceGroupFullOutPut
}
