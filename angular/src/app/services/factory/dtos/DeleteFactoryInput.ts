export class DeleteFactoryInput implements IDeleteFactoryInput {
    name: string;
    id: number;
}

export interface IDeleteFactoryInput {
    name: string;
    id: number;
}
