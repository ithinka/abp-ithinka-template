import { isObject } from "util";

export class FactoryFullOutPut implements IFactoryFullOutPut{
    id: number;
    name: string;

    constructor(data?: IFactoryFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
            }
        }
    }

    static fromJS(data: any): FactoryFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new FactoryFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        return data; 
    }

    clone(): FactoryFullOutPut {
        const json = this.toJSON();
        let result = new FactoryFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IFactoryFullOutPut{
    id:number;
    name:string;
}