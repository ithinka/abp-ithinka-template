import { NgModule } from '@angular/core';

import { DepartmentService } from './department/department.service';
import { FactoryService } from './factory/factory.service';
import { UtilsService } from './common/utils.service';
import { ModalManagerService } from './common/modal-manager.service';
import { DeviceGroupService } from './device-group/device-group.service';
import { DeviceService } from './device/device.service';
import { StopService } from './stop/stop.service';
import { StopTypeService } from './stop-type/stop-type.service';

@NgModule({
  providers: [
    UtilsService,
    ModalManagerService,
    DepartmentService,
    FactoryService,
    DeviceGroupService,
    DeviceService,
    StopService,
    StopTypeService
  ]
})
export class IthinkaServiceModule { }
