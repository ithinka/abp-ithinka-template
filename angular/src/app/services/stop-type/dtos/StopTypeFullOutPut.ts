import { isObject } from "util";

export class StopTypeFullOutPut implements IStopTypeFullOutPut {
    name:string;
    id: number;

    constructor(data?: IStopTypeFullOutPut) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if(isObject(data))
            {
                this.id = data.id;
                this.name = data.name;
            }
        }
    }

    static fromJS(data: any): StopTypeFullOutPut {
        data = typeof data === 'object' ? data : {};
        let result = new StopTypeFullOutPut();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        return data; 
    }

    clone(): StopTypeFullOutPut {
        const json = this.toJSON();
        let result = new StopTypeFullOutPut();
        result.init(json);
        return result;
    }
}

export interface IStopTypeFullOutPut {
    name: string | undefined;
    id:number
}
