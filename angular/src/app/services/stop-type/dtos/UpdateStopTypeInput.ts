export class UpdateStopTypeInput implements IUpdateStopTypeInput {
    name: string;
    id: number;
}

export interface IUpdateStopTypeInput {
    name: string;
    id: number;
}
