/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StopTypeService } from './stop-type.service';

describe('Service: StopType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StopTypeService]
    });
  });

  it('should ...', inject([StopTypeService], (service: StopTypeService) => {
    expect(service).toBeTruthy();
  }));
});
