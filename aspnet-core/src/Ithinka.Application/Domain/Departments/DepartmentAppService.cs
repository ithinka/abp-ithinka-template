﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Departments.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Ithinka.Domain.Departments
{
    [AbpAuthorize(PermissionNames.Department)]
    public class DepartmentAppService : IthinkaAppServiceBase, IDepartmentAppService
    {
        private readonly IEntityManager _entityManager;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DepartmentAppService(
            IEntityManager entityManager,
            IDepartmentRepository departmentRepository,
            IUnitOfWorkManager unitOfWorkManager) 
        {
            _entityManager = entityManager;
            _departmentRepository = departmentRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Department_Create)]
        public async Task<DepartmentFullOutPut> CreateAsync(CreateDepartmentInput input)
        {
            var department = new Department()
            {
                Name = input.Name,
                FactoryId = input.Factory?.Id
            };

            await _departmentRepository.InsertAsync(department);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Department_Get)]
        public async Task<DepartmentFullOutPut> GetAsync(GetDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);
            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Department_GetList)]
        public async Task<List<DepartmentFullOutPut>> GetListAsync()
        {
            try
            {
                var departmentList = await  _departmentRepository.GetAllListAsync();

                var outPut = ObjectMapper.Map<List<DepartmentFullOutPut>>(departmentList);
                return outPut;
            }
            catch (System.Exception ex)
            {
                throw new EntityNotFoundException(typeof(Department), ex);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Department_Delete)]
        public async Task DeleteAsync(DeleteDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);
            await _departmentRepository.DeleteAsync(department.Id);

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Department_Update)]
        public async Task<DepartmentFullOutPut> UpdateAsync(UpdateDepartmentInput input)
        {
            var department = await _entityManager.GetDepartmentAsync(input.Id);
            department.Name = input.Name.IsNullOrEmpty() ? department.Name : input.Name; ;
            department.FactoryId = input.Factory?.Id;

            await _departmentRepository.UpdateAsync(department);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DepartmentFullOutPut>(department);
        }
    }
}
