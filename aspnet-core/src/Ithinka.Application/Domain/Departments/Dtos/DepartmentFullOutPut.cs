﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Factories.Dtos;

namespace Ithinka.Domain.Departments.Dtos
{
    public class DepartmentFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }

        public FactoryPartOutPut Factory { get; set; }
    }
}
