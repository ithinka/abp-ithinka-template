﻿using Ithinka.Domain.Departments.Dtos;

namespace Ithinka.Domain.DeviceGroups.Dtos
{
    public class CreateDeviceGroupInput
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }

        public DepartmentPartOutPut Department { get; set; }
    }
}
