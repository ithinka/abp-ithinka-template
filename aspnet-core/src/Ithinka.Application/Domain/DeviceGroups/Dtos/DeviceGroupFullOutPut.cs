﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Departments.Dtos;

namespace Ithinka.Domain.DeviceGroups.Dtos
{
    public class DeviceGroupFullOutPut : EntityDto<int>
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }

        public DepartmentFullOutPut Department { get; set; }
    }
}
