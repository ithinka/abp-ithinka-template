﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.Departments.Dtos;

namespace Ithinka.Domain.DeviceGroups.Dtos
{
    public class UpdateDeviceGroupInput : EntityDto<int>
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }

        public DepartmentPartOutPut Department { get; set; }
    }
}
