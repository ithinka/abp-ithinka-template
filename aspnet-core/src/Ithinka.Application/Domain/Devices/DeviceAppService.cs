﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Devices.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Devices
{
    public class DeviceAppService : IthinkaAppServiceBase, IDeviceAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IDeviceRepository _deviceRepository;

        public DeviceAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IDeviceRepository deviceRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _deviceRepository = deviceRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_Create)]
        public async Task<DeviceFullOutPut> CreateAsync(CreateDeviceInput input)
        {
            var device = new Device()
            {
                Name = input.Name,
                DeviceNo = input.DeviceNo,
                IsOpened = input.IsOpened,
                DeviceGroupId = input.DeviceGroup?.Id
            };

            await _deviceRepository.InsertAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_Get)]
        public async Task<DeviceFullOutPut> GetAsync(GetDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_GetList)]
        public async Task<List<DeviceFullOutPut>> GetListAsync()
        {
            try
            {
                var devices = await _deviceRepository.GetAllListAsync();
                return ObjectMapper.Map<List<DeviceFullOutPut>>(devices);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(typeof(Device), ex);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_Delete)]
        public async Task DeleteAsync(DeleteDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            await _deviceRepository.DeleteAsync(device.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.Device_Update)]
        public async Task<DeviceFullOutPut> UpdateAsync(UpdateDeviceInput input)
        {
            var device = await _entityManager.GetDeviceAsync(input.Id);

            device.Name = input.Name.IsNullOrEmpty() ? device.Name : input.Name;
            device.DeviceNo = input.DeviceNo.IsNullOrEmpty() ? device.DeviceNo : input.DeviceNo;
            device.IsOpened = input.IsOpened;
            device.DeviceGroupId = input.DeviceGroup?.Id;

            await _deviceRepository.UpdateAsync(device);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<DeviceFullOutPut>(device);
        }
    }
}
