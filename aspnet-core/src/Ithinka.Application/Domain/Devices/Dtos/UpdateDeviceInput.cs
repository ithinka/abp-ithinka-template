﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.DeviceGroups.Dtos;

namespace Ithinka.Domain.Devices.Dtos
{
    public class UpdateDeviceInput : EntityDto<int>
    {
        public string Name { get; set; }
        public string DeviceNo { get; set; }
        public bool IsOpened { get; set; }
        public DeviceGroupPartOutPut DeviceGroup { get; set; }
    }
}
