﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.Factories.Dtos
{
    public class UpdateFactoryInput : EntityDto<int>
    {
        public string Name { get; set; }
    }
}
