﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Factories.Dtos;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Factories
{
    [AbpAuthorize(PermissionNames.Factory)]
    public class FactoryAppService : IthinkaAppServiceBase, IFactoryAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IFactoryRepository _factoryRepository;

        public FactoryAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IFactoryRepository factoryRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _factoryRepository = factoryRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Factory_Create)]
        public async Task<FactoryFullOutPut> CreateAsync(CreateFactoryInput input)
        {
            var factory = new Factory()
            {
                Name = input.Name
            };

            await _factoryRepository.InsertAsync(factory);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Factory_Get)]
        public async Task<FactoryFullOutPut> GetAsync(GetFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Factory_GetList)]
        public async Task<List<FactoryFullOutPut>> GetListAsync()
        {
            try
            {
                var factories = await _factoryRepository.GetAllListAsync();
                return ObjectMapper.Map<List<FactoryFullOutPut>>(factories);
            }
            catch (Exception ex)
            {
                throw new EntityNotFoundException(typeof(Department), ex);
            }
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Factory_Delete)]
        public async Task DeleteAsync(DeleteFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);
            await _factoryRepository.DeleteAsync(factory.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.Factory_Update)]
        public async Task<FactoryFullOutPut> UpdateAsync(UpdateFactoryInput input)
        {
            var factory = await _entityManager.GetFactoryAsync(input.Id);
            factory.Name = input.Name.IsNullOrEmpty() ? factory.Name : input.Name;

            await _factoryRepository.UpdateAsync(factory);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<FactoryFullOutPut>(factory);
        }
    }
}
