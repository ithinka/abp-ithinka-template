﻿using Abp.Application.Services;
using Ithinka.Domain.Factories.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Factories
{
    public interface IFactoryAppService : IApplicationService
    {
        #region Async Methods
        Task<FactoryFullOutPut> CreateAsync(CreateFactoryInput input);
        Task<FactoryFullOutPut> GetAsync(GetFactoryInput input);
        Task<List<FactoryFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteFactoryInput input);
        Task<FactoryFullOutPut> UpdateAsync(UpdateFactoryInput input);
        #endregion Async Methods
    }
}
