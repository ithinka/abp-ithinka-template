﻿using Abp.Application.Services.Dto;

namespace Ithinka.Domain.StopTypes.Dtos
{
    public class UpdateStopTypeInput: EntityDto<int>
    {
        public string Name { get; set; }
    }
}
