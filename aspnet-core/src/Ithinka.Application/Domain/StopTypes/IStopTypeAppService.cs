﻿using Ithinka.Domain.StopTypes.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.StopTypes
{
    interface IStopTypeTypeAppService
    {
        #region Async StopType
        Task<StopTypeFullOutPut> CreateAsync(CreateStopTypeInput input);
        Task<StopTypeFullOutPut> GetAsync(GetStopTypeInput input);
        Task<List<StopTypeFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteStopTypeInput input);
        Task<StopTypeFullOutPut> UpdateAsync(UpdateStopTypeInput input);
        #endregion Async StopType
    }
}
