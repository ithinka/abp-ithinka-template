﻿using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Extensions;
using Ithinka.Authorization;
using Ithinka.Domain.Entities;
using Ithinka.Domain.StopTypes.Dtos;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Manager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.StopTypes
{
    public class StopTypeAppService : IthinkaAppServiceBase, IStopTypeTypeAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IEntityManager _entityManager;
        private readonly IStopTypeRepository _stopTypeRepository;

        public StopTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEntityManager entityManager,
            IStopTypeRepository stopTypeRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _entityManager = entityManager;
            _stopTypeRepository = stopTypeRepository;
        }

        [HttpPost]
        [AbpAuthorize(PermissionNames.StopType_Create)]
        public async Task<StopTypeFullOutPut> CreateAsync(CreateStopTypeInput input)
        {
            var stopType = new StopType()
            {
                Name = input.Name
            };

            await _stopTypeRepository.InsertAsync(stopType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.StopType_Get)]
        public async Task<StopTypeFullOutPut> GetAsync(GetStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.StopType_GetList)]
        public async Task<List<StopTypeFullOutPut>> GetListAsync()
        {
            try
            {
                var stopTypes = await _stopTypeRepository.GetAllListAsync();
                return ObjectMapper.Map<List<StopTypeFullOutPut>>(stopTypes);
            }
            catch (Exception ex)
            {

                throw new EntityNotFoundException(typeof(StopType), ex);
            }
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.StopType_Delete)]
        public async Task DeleteAsync(DeleteStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            await _stopTypeRepository.DeleteAsync(stopType.Id);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }
        [HttpPost]
        [AbpAuthorize(PermissionNames.StopType_Update)]
        public async Task<StopTypeFullOutPut> UpdateAsync(UpdateStopTypeInput input)
        {
            var stopType = await _entityManager.GetStopTypeAsync(input.Id);

            stopType.Name = input.Name.IsNullOrEmpty() ? stopType.Name : input.Name;

            await _stopTypeRepository.UpdateAsync(stopType);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            return ObjectMapper.Map<StopTypeFullOutPut>(stopType);
        }
    }
}
