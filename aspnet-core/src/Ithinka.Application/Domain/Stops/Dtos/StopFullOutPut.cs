﻿using Abp.Application.Services.Dto;
using Ithinka.Domain.StopTypes.Dtos;

namespace Ithinka.Domain.Stops.Dtos
{
    public class StopFullOutPut: EntityDto<int>
    {
        public string Name { get; set; }
        public int? Duration { get; set; }

        public StopTypeFullOutPut StopType { get; set; }
    }
}
