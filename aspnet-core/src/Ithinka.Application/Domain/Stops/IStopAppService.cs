﻿using Abp.Application.Services;
using Ithinka.Domain.Stops.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ithinka.Domain.Stops
{
    public interface IStopAppService : IApplicationService
    {
        #region Async Methods
        Task<StopFullOutPut> CreateAsync(CreateStopInput input);
        Task<StopFullOutPut> GetAsync(GetStopInput input);
        Task<List<StopFullOutPut>> GetListAsync();
        Task DeleteAsync(DeleteStopInput input);
        Task<StopFullOutPut> UpdateAsync(UpdateStopInput input);
        #endregion Async Methods
    }
}
