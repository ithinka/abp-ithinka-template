﻿using Abp.Domain.Entities;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Ithinka.Domain.Entities;
using Ithinka.EntityFrameworkCore.Repositories;
using Ithinka.Services;
using Castle.Core.Logging;
using System.Threading.Tasks;

namespace Ithinka.Manager
{
    public class EntityManager : AbpAppServiceBase, IEntityManager
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IFactoryRepository _factoryRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IDeviceGroupRepository _deviceGroupRepository;
        private readonly IStopRepository _stopRepository;
        private readonly IStopTypeRepository _stopTypeRepository;

        public EntityManager(
            IAbpSession abpSession, 
            ILocalizationManager localizationManager, 
            ILogger logger, 
            IUnitOfWorkManager unitOfWorkManager, 
            IObjectMapper objectMapper,
            IDepartmentRepository departmentRepository,
            IFactoryRepository factoryRepository,
            IDeviceRepository deviceRepository,
            IDeviceGroupRepository deviceGroupRepository,
            IStopRepository stopRepository,
            IStopTypeRepository stopTypeRepository) 
            : base(abpSession, localizationManager, logger, unitOfWorkManager, objectMapper)
        {
            _departmentRepository = departmentRepository;
            _factoryRepository = factoryRepository;
            _deviceRepository = deviceRepository;
            _deviceGroupRepository = deviceGroupRepository;
            _stopRepository = stopRepository;
            _stopTypeRepository = stopTypeRepository;
        }

        public async Task<Department> GetDepartmentAsync(int departmentId)
        {
            var department = await _departmentRepository.FirstOrDefaultAsync(d =>
               d.Id == departmentId 
               // Kullanıcı sadece kendi verilerini görmek istiyorsa bu kod aktif edilmeli
               //&& d.CreatorUserId == AbpSession.UserId
               );
            if (department == null)
            {
                throw new EntityNotFoundException(typeof(Department), departmentId);
            }

            return department;
        }

        public async Task<Factory> GetFactoryAsync(int factoryId)
        {
            var factory = await _factoryRepository.FirstOrDefaultAsync(f => f.Id == factoryId);

            if (factory == null)
            {
                throw new EntityNotFoundException(typeof(Factory), factoryId);
            }

            return factory;
        }

        public async Task<Device> GetDeviceAsync(int deviceId)
        {
            var device = await _deviceRepository.FirstOrDefaultAsync(d => d.Id == deviceId);

            if (device == null)
            {
                throw new EntityNotFoundException(typeof(Device), deviceId);
            }

            return device;
        }

        public async Task<DeviceGroup> GetDeviceGroupAsync(int deviceGroupId)
        {
            var deviceGroup = await _deviceGroupRepository.FirstOrDefaultAsync(d => d.Id == deviceGroupId);

            if (deviceGroup == null)
            {
                throw new EntityNotFoundException(typeof(DeviceGroup), deviceGroupId);
            }

            return deviceGroup;
        }

        public async Task<Stop> GetStopAsync(int stopId)
        {
            var stop = await _stopRepository.FirstOrDefaultAsync(d => d.Id == stopId);

            if (stop == null)
            {
                throw new EntityNotFoundException(typeof(Stop), stopId);
            }

            return stop;
        }

        public async Task<StopType> GetStopTypeAsync(int stopTypeId)
        {
            var stopType = await _stopTypeRepository.FirstOrDefaultAsync(d => d.Id == stopTypeId);

            if (stopType == null)
            {
                throw new EntityNotFoundException(typeof(StopType), stopTypeId);
            }

            return stopType;
        }
    }
}
