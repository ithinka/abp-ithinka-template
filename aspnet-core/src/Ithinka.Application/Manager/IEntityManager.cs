﻿using Abp.Dependency;
using Ithinka.Domain.Entities;
using System.Threading.Tasks;

namespace Ithinka.Manager
{
    public interface IEntityManager : ITransientDependency
    {
        Task<Department> GetDepartmentAsync(int departmentId);
        Task<Factory> GetFactoryAsync(int factoryId);
        Task<Device> GetDeviceAsync(int deviceId);
        Task<DeviceGroup> GetDeviceGroupAsync(int deviceGroupId);
        Task<Stop> GetStopAsync(int stopId);
        Task<StopType> GetStopTypeAsync(int stopTypeId);
    }
}
