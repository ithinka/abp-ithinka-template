﻿using Ithinka.Domain.Departments.Dtos;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Factories.Dtos;
using AutoMapper;
using Ithinka.Domain.Devices.Dtos;
using Ithinka.Domain.DeviceGroups.Dtos;
using Ithinka.Domain.Stops.Dtos;
using Ithinka.Domain.StopTypes.Dtos;

namespace Ithinka.Manager
{
    public class MapperManager
    {
        public static void DtosToDomain(IMapperConfigurationExpression cfg)
        {
            // Departments
            cfg.CreateMap<Department, CreateDepartmentInput>();
            cfg.CreateMap<Department, GetDepartmentInput>();
            cfg.CreateMap<Department, DeleteDepartmentInput>();
            cfg.CreateMap<Department, UpdateDepartmentInput>();
            cfg.CreateMap<Department, DepartmentFullOutPut>();
            cfg.CreateMap<Department, DepartmentPartOutPut>();

            // Factories
            cfg.CreateMap<Factory, CreateFactoryInput>();
            cfg.CreateMap<Factory, GetFactoryInput>();
            cfg.CreateMap<Factory, DeleteFactoryInput>();
            cfg.CreateMap<Factory, UpdateFactoryInput>();
            cfg.CreateMap<Factory, FactoryFullOutPut>();
            cfg.CreateMap<Factory, FactoryPartOutPut>();

            // Devices
            cfg.CreateMap<Device, CreateDeviceInput>();
            cfg.CreateMap<Device, GetDeviceInput>();
            cfg.CreateMap<Device, DeleteDeviceInput>();
            cfg.CreateMap<Device, UpdateDeviceInput>();
            cfg.CreateMap<Device, DeviceFullOutPut>();

            // DeviceGroups
            cfg.CreateMap<DeviceGroup, CreateDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, GetDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, DeleteDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, UpdateDeviceGroupInput>();
            cfg.CreateMap<DeviceGroup, DeviceGroupFullOutPut>();

            // Stops
            cfg.CreateMap<Stop, CreateStopInput>();
            cfg.CreateMap<Stop, GetStopInput>();
            cfg.CreateMap<Stop, DeleteStopInput>();
            cfg.CreateMap<Stop, UpdateStopInput>();
            cfg.CreateMap<Stop, StopFullOutPut>();

            // StopTypes
            cfg.CreateMap<StopType, CreateStopTypeInput>();
            cfg.CreateMap<StopType, GetStopTypeInput>();
            cfg.CreateMap<StopType, DeleteStopTypeInput>();
            cfg.CreateMap<StopType, UpdateStopTypeInput>();
            cfg.CreateMap<StopType, StopTypeFullOutPut>();
        }
    }
}
