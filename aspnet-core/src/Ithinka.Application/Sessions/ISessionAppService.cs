﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Ithinka.Sessions.Dto;

namespace Ithinka.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
