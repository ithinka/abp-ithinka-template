using System.ComponentModel.DataAnnotations;

namespace Ithinka.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}