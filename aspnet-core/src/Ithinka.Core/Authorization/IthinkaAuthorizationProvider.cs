﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Ithinka.Authorization
{
    public class IthinkaAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"))
                .CreateChildPermission(PermissionNames.Pages_Users_Menu, L("Users_Munu"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            // Departments
            context.CreatePermission(PermissionNames.Department, L("Permission_Department"));
            context.CreatePermission(PermissionNames.Department_Create, L("Permission_Department_Create"));
            context.CreatePermission(PermissionNames.Department_Get, L("Permission_Department_Get"));
            context.CreatePermission(PermissionNames.Department_GetList, L("Permission_Department_GetList"));
            context.CreatePermission(PermissionNames.Department_Delete, L("Permission_Department_Delete"));
            context.CreatePermission(PermissionNames.Department_Update, L("Permission_Department_Update"));

            // Factories
            context.CreatePermission(PermissionNames.Factory, L("Permission_Factory"));
            context.CreatePermission(PermissionNames.Factory_Create, L("Permission_Factory_Create"));
            context.CreatePermission(PermissionNames.Factory_Get, L("Permission_Factory_Get"));
            context.CreatePermission(PermissionNames.Factory_GetList, L("Permission_Factory_GetList"));
            context.CreatePermission(PermissionNames.Factory_Delete, L("Permission_Factory_Delete"));
            context.CreatePermission(PermissionNames.Factory_Update, L("Permission_Factory_Update"));

            // Devices
            context.CreatePermission(PermissionNames.Device, L("Permission_Device"));
            context.CreatePermission(PermissionNames.Device_Create, L("Permission_Device_Create"));
            context.CreatePermission(PermissionNames.Device_Get, L("Permission_Device_Get"));
            context.CreatePermission(PermissionNames.Device_GetList, L("Permission_Device_GetList"));
            context.CreatePermission(PermissionNames.Device_Delete, L("Permission_Device_Delete"));
            context.CreatePermission(PermissionNames.Device_Update, L("Permission_Device_Update"));

            // DeviceGroups
            context.CreatePermission(PermissionNames.DeviceGroup, L("Permission_DeviceGroup"));
            context.CreatePermission(PermissionNames.DeviceGroup_Create, L("Permission_DeviceGroup_Create"));
            context.CreatePermission(PermissionNames.DeviceGroup_Get, L("Permission_DeviceGroup_Get"));
            context.CreatePermission(PermissionNames.DeviceGroup_GetList, L("Permission_DeviceGroup_GetList"));
            context.CreatePermission(PermissionNames.DeviceGroup_Delete, L("Permission_DeviceGroup_Delete"));
            context.CreatePermission(PermissionNames.DeviceGroup_Update, L("Permission_DeviceGroup_Update"));

            // Stops
            context.CreatePermission(PermissionNames.Stop, L("Permission_Stop"));
            context.CreatePermission(PermissionNames.Stop_Create, L("Permission_Stop_Create"));
            context.CreatePermission(PermissionNames.Stop_Get, L("Permission_Stop_Get"));
            context.CreatePermission(PermissionNames.Stop_GetList, L("Permission_Stop_GetList"));
            context.CreatePermission(PermissionNames.Stop_Delete, L("Permission_Stop_Delete"));
            context.CreatePermission(PermissionNames.Stop_Update, L("Permission_Stop_Update"));

            // StopTypes
            context.CreatePermission(PermissionNames.StopType, L("Permission_StopType"));
            context.CreatePermission(PermissionNames.StopType_Create, L("Permission_StopType_Create"));
            context.CreatePermission(PermissionNames.StopType_Get, L("Permission_StopType_Get"));
            context.CreatePermission(PermissionNames.StopType_GetList, L("Permission_StopType_GetList"));
            context.CreatePermission(PermissionNames.StopType_Delete, L("Permission_StopType_Delete"));
            context.CreatePermission(PermissionNames.StopType_Update, L("Permission_StopType_Update"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, IthinkaConsts.LocalizationSourceName);
        }
    }
}
