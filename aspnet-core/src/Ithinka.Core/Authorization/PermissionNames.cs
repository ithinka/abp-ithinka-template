﻿namespace Ithinka.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";
        public const string Pages_Users_Menu = "Pages.Users.Menu";

        public const string Pages_Roles = "Pages.Roles";

        #region Departments
        public const string Department = "Department";
        public const string Department_Create = "Department.Create";
        public const string Department_Get = "Department.Get";
        public const string Department_GetList = "Department.GetList";
        public const string Department_Delete = "Department.Delete";
        public const string Department_Update = "Department.Update";
        #endregion

        #region Factories
        public const string Factory = "Factory";
        public const string Factory_Create = "Factory_Create";
        public const string Factory_Get = "Factory_Get";
        public const string Factory_GetList = "Factory_GetList";
        public const string Factory_Delete = "Factory_Delete";
        public const string Factory_Update = "Factory_Update";
        #endregion

        #region Devices
        public const string Device = "Device";
        public const string Device_Create = "Device_Create";
        public const string Device_Get = "Device_Get";
        public const string Device_GetList = "Device_GetList";
        public const string Device_Delete = "Device_Delete";
        public const string Device_Update = "Device_Update";
        #endregion

        #region DeviceGroups
        public const string DeviceGroup = "DeviceGroup";
        public const string DeviceGroup_Create = "DeviceGroup_Create";
        public const string DeviceGroup_Get = "DeviceGroup_Get";
        public const string DeviceGroup_GetList = "DeviceGroup_GetList";
        public const string DeviceGroup_Delete = "DeviceGroup_Delete";
        public const string DeviceGroup_Update = "DeviceGroup_Update";
        #endregion

        #region Stops
        public const string Stop = "Stop";
        public const string Stop_Create = "Stop_Create";
        public const string Stop_Get = "Stop_Get";
        public const string Stop_GetList = "Stop_GetList";
        public const string Stop_Delete = "Stop_Delete";
        public const string Stop_Update = "Stop_Update";
        #endregion

        #region StopType
        public const string StopType = "StopType";
        public const string StopType_Create = "StopType_Create";
        public const string StopType_Get = "StoTypep_Get";
        public const string StopType_GetList = "StopType_GetList";
        public const string StopType_Delete = "StopType_Delete";
        public const string StopType_Update = "StopType_Update";
        #endregion
    }
}
