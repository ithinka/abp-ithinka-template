﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class DeviceConfiguration : IEntityTypeConfiguration<Device>
    {
        public void Configure(EntityTypeBuilder<Device> builder)
        {
            #region Properties
            builder.ToTable("Device");

            builder.HasKey(device => device.Id);

            builder.Property(device => device.Name)
                .HasColumnName("Name")
                .HasMaxLength(100);

            builder.Property(device => device.DeviceNo)
                .HasColumnName("DeviceNo")
                .HasMaxLength(50);

            builder.Property(device => device.IsOpened)
                .HasColumnName("IsOpened");
            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(deviceGroup => deviceGroup.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}
