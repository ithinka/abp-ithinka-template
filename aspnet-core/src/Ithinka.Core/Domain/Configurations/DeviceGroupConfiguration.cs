﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class DeviceGroupConfiguration : IEntityTypeConfiguration<DeviceGroup>
    {
        public void Configure(EntityTypeBuilder<DeviceGroup> builder)
        {
            #region Properties
            builder.ToTable("DeviceGroup");

            builder.HasKey(deviceGroup => deviceGroup.Id);

            builder.Property(deviceGroup => deviceGroup.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            builder.Property(deviceGroup => deviceGroup.IPAddress)
                .HasColumnName("IPAddress")
                .HasMaxLength(15);
            #endregion Properties

            #region Relations
            builder.HasMany<Device>(deviceGroup => deviceGroup.Devices)
                .WithOne(device => device.DeviceGroup)
                .HasForeignKey(device => device.DeviceGroupId)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(deviceGroup => deviceGroup.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}
