﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class DeviceGroupStopConfiguration : IEntityTypeConfiguration<DeviceGroupStop>
    {
        public void Configure(EntityTypeBuilder<DeviceGroupStop> builder)
        {
            builder.ToTable("DeviceGroupStop");

            builder.HasKey(dgs => new { dgs.DeviceGroupId, dgs.StopId });

            #region Relations
            builder
                .HasOne<DeviceGroup>(dgs => dgs.DeviceGroup)
                .WithMany(deviceGroup => deviceGroup.DeviceGroupStops)
                .HasForeignKey(dgs => dgs.DeviceGroupId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder
                .HasOne<Stop>(dgs => dgs.Stop)
                .WithMany(stop => stop.DeviceGroupStops)
                .HasForeignKey(dgs => dgs.StopId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            #endregion Relations
        }
    }
}
