﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class StopConfiguration : IEntityTypeConfiguration<Stop>
    {
        public void Configure(EntityTypeBuilder<Stop> builder)
        {
            #region Properties
            builder.ToTable("Stop");

            builder.HasKey(stop => stop.Id);

            builder.Property(stop => stop.Name)
                .HasColumnName("Name")
                .HasMaxLength(150);

            builder.Property(stop => stop.Duration)
                .HasColumnName("Duration");
            #endregion Properties

            #region Relations

            #endregion Relations

            #region OptimisticLockField
            builder.Property(stop => stop.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}
