﻿using Ithinka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ithinka.Domain.Configurations
{
    public class StopTypeConfiguration : IEntityTypeConfiguration<StopType>
    {
        public void Configure(EntityTypeBuilder<StopType> builder)
        {
            #region Properties
            builder.ToTable("StopType");

            builder.HasKey(stopType => stopType.Id);

            builder.Property(stopType => stopType.Name)
                .HasColumnName("Name")
                .HasMaxLength(100);
            #endregion Properties

            #region Relations
            builder.HasMany<Stop>(stopType => stopType.Stops)
                .WithOne(stop => stop.StopType)
                .HasForeignKey(stop => stop.StopTypeId)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion Relations

            #region OptimisticLockField
            builder.Property(stopType => stopType.RowVersion)
                .IsRowVersion();
            #endregion OptimisticLockField
        }
    }
}
