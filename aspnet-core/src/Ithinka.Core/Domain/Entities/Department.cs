﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class Department : FullAuditedEntity<int>
    {
        #region Constructor
        public Department()
        {
            DeviceGroups = new HashSet<DeviceGroup>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        #endregion Properties   

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? FactoryId { get; set; }
        public virtual Factory Factory { get; set; }

        public virtual ICollection<DeviceGroup> DeviceGroups { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}
