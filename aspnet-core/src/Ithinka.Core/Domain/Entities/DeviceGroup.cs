﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace Ithinka.Domain.Entities
{
    public class DeviceGroup : FullAuditedEntity<int>
    {
        #region Constructor
        public DeviceGroup()
        {
            Devices = new HashSet<Device>();
            DeviceGroupStops = new HashSet<DeviceGroupStop>();
        }
        #endregion Constructor

        #region Properties
        public string Name { get; set; }
        public string IPAddress { get; set; }
        #endregion Properties

        #region Relations
        #region One To One Relations

        #endregion One To One Relations

        #region One To Many Relations
        public int? DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<DeviceGroupStop> DeviceGroupStops { get; set; }
        #endregion One To Many Relations

        #region Many To Many Relations

        #endregion Many To Many Relations
        #endregion Relations

        #region OptimisticLockField
        public byte[] RowVersion { get; set; }
        #endregion OptimisticLockField
    }
}
