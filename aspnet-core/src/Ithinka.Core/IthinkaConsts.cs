﻿namespace Ithinka
{
    public class IthinkaConsts
    {
        public const string LocalizationSourceName = "Ithinka";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
