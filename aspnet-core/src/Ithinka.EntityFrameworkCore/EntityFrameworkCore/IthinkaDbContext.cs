﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Ithinka.Authorization.Roles;
using Ithinka.Authorization.Users;
using Ithinka.MultiTenancy;
using Ithinka.Domain.Entities;
using Ithinka.Domain.Configurations;

namespace Ithinka.EntityFrameworkCore
{
    public class IthinkaDbContext : AbpZeroDbContext<Tenant, Role, User, IthinkaDbContext>
    {
        /* Define a DbSet for each entity of the application */
        #region DBSet
        public DbSet<Department> Departments { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceGroup> DeviceGroups { get; set; }
        public DbSet<DeviceGroupStop> DeviceGroupStops { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopType> StopTypes { get; set; }
        #endregion DBSet
        public IthinkaDbContext(DbContextOptions<IthinkaDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .ApplyConfiguration(new DepartmentConfiguration())
                .ApplyConfiguration(new DeviceConfiguration())
                .ApplyConfiguration(new DeviceGroupConfiguration())
                .ApplyConfiguration(new DeviceGroupStopConfiguration())
                .ApplyConfiguration(new FactoryConfiguration())
                .ApplyConfiguration(new StopConfiguration())
                .ApplyConfiguration(new StopTypeConfiguration());
        }
    }
}
