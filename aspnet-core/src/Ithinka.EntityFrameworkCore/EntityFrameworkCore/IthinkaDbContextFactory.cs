﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Ithinka.Configuration;
using Ithinka.Web;

namespace Ithinka.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class IthinkaDbContextFactory : IDesignTimeDbContextFactory<IthinkaDbContext>
    {
        public IthinkaDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<IthinkaDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            builder.UseLazyLoadingProxies();

            IthinkaDbContextConfigurer.Configure(builder, configuration.GetConnectionString(IthinkaConsts.ConnectionStringName));

            return new IthinkaDbContext(builder.Options);
        }
    }
}
