﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Events
{
    public class DeviceGroupDeletingHandler : IEventHandler<EntityDeletingEventData<DeviceGroup>>, ITransientDependency
    {
        private readonly IDeviceRepository _deviceRepository;

        public DeviceGroupDeletingHandler(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public void HandleEvent(EntityDeletingEventData<DeviceGroup> eventData)
        {
            var deviceGroup = eventData.Entity;

            foreach (var device in deviceGroup.Devices)
            {
                _deviceRepository.Delete(device);
            }
        }
    }
}
