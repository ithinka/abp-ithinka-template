﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories.Events
{
    public class StopTypeDeletingHandler : IEventHandler<EntityDeletingEventData<StopType>>, ITransientDependency
    {
        private readonly IStopRepository _stopRepository;

        public StopTypeDeletingHandler(IStopRepository stopRepository)
        {
            _stopRepository = stopRepository;
        }

        public void HandleEvent(EntityDeletingEventData<StopType> eventData)
        {
            var stopType = eventData.Entity;

            foreach (var stop in stopType.Stops)
            {
                _stopRepository.Delete(stop);
            }
        }
    }
}
