﻿using Abp.EntityFrameworkCore;
using Ithinka.Domain.Entities;

namespace Ithinka.EntityFrameworkCore.Repositories
{
    public class StopRepository : IthinkaRepositoryBase<Stop, int>, IStopRepository
    {
        public StopRepository(IDbContextProvider<IthinkaDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
        }
    }
}
