using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Ithinka.Controllers
{
    public abstract class IthinkaControllerBase: AbpController
    {
        protected IthinkaControllerBase()
        {
            LocalizationSourceName = IthinkaConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
